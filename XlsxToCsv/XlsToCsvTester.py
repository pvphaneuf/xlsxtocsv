__author__ = 'pphaneuf'

#Only have xlrd installed with Python 2.7
import xlrd
import csv


def excel_to_csv_all_sheet():

    wb = xlrd.open_workbook('PNDD_Candidate_mutations.xlsx')

    your_csv_file = open('your_csv_file.csv', 'wb')
    wr = csv.writer(your_csv_file, quoting=csv.QUOTE_ALL)

    for sheet in wb.sheets():
        for row in xrange(sheet.nrows):
            # print sheet.row_values(row)
            wr.writerow(sheet.row_values(row))

    your_csv_file.close()


def excel_to_csv_single_sheet(sheet_name):

    wb = xlrd.open_workbook('PNDD_Candidate_mutations.xlsx')
    sheet = wb.sheet_by_name(sheet_name)
    your_csv_file = open('your_csv_file.csv', 'wb')
    wr = csv.writer(your_csv_file, quoting=csv.QUOTE_ALL)

    for row in xrange(sheet.nrows):
        wr.writerow(sheet.row_values(row))
        # print sh.row_values(row)

    your_csv_file.close()


def main():

    excel_to_csv_single_sheet('DOM_Novo')


if __name__ == "__main__":
    main()